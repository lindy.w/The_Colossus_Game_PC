﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof (AudioSource))]

public class FlashLight : MonoBehaviour {

	public Light flashlightLightSource;
	public bool lightOn = false;
	public float lightDrain = 1.0f; //minus 1 each second

	//new
	private static float lightLife = 0.0f;
	private static float maxLightLife = 10.0f; //float countdown as seconds

	private static float matchPower = 10.0f; //charge up 10 per 1 matchbox

	public AudioClip soundTurnOn, soundTurnOff;

	//for references
	InventoryController inventoryControllerRef;
	public GameObject inventory;

	// Use this for initialization
	void Start () {
		//initialized as ZERO at the beginning
		lightLife = 0; //set 0 ;
		flashlightLightSource = GetComponent<Light>();

		inventoryControllerRef = inventory.GetComponent<InventoryController>();
	}

	public void AlterEnergy (float amount) {
		lightLife = Mathf.Clamp (lightLife + matchPower, 0, 100);
		Debug.Log ("lightLife: " + lightLife);
		if (lightLife > 10) {
			lightLife = maxLightLife;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//adjust the light life brightness//
		if (lightOn && lightLife >= 0)
		{
			lightLife -= Time.deltaTime * lightDrain;
		}
		if (lightOn && lightLife <= 10 && lightLife > 9)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 10;
		}
		if (lightOn && lightLife <= 9 && lightLife > 8)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 9;
		}
		if (lightOn && lightLife <= 8 && lightLife > 7)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 8;
		}
		if (lightOn && lightLife <= 7 && lightLife > 6)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 7;
		}
		if (lightOn && lightLife <= 6 && lightLife > 5)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 6;
		}
		if (lightOn && lightLife <= 5 && lightLife > 4)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 5;
		}
		if (lightOn && lightLife <= 4 && lightLife > 3)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 4;
		}
		if (lightOn && lightLife <= 3 && lightLife > 2)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 3;
		}
		if (lightOn && lightLife <= 2 && lightLife > 1)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 2;
		}
		if (lightOn && lightLife <= 1 && lightLife > 0)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 1;
		}
		if (lightOn && lightLife <= 0)
		{
			flashlightLightSource.GetComponent<Light>().intensity = 0;
		}

		if (lightLife <= 0)
		{
			lightLife = 0;
			lightOn = false; //turn off the light when running out
		}

		//Press "F" button to turn on/off the light
		if (Input.GetKeyUp(KeyCode.F)) {
			//add lightlife 10 if and deduct 1 matchbox from inventory
			bool addLight = inventoryControllerRef.useMatch();
				if (addLight == true) 
					AlterEnergy(10f); //add 10 light life to flashlight
				
			toggleFlashlight ();
			toggleFlashlightSFX ();

			if (!lightOn) {
				lightOn = true;
			}
			Debug.Log("lightLife: " + lightLife);
			Debug.Log("Intensity: " + flashlightLightSource.GetComponent<Light>().intensity);
		}
		//Debug.Log("lightLife: " + lightLife);
		//Debug.Log("Intensity: " + flashlightLightSource.GetComponent<Light>().intensity);
	}

	void toggleFlashlight()
	{
		if (lightOn) {
			flashlightLightSource.enabled = false;
		} else {
			flashlightLightSource.enabled = true;
		}
		
	}

	void toggleFlashlightSFX()
	{
		if (flashlightLightSource.enabled) {
			GetComponent<AudioSource> ().clip = soundTurnOn;
			//Debug.Log ("Light Life: " + lightLife);
		} else {
			GetComponent<AudioSource> ().clip = soundTurnOff;
		}
		GetComponent<AudioSource>().Play();
	}
		

}
