﻿using UnityEngine;
using System.Collections;

public class ObjectPhysics : MonoBehaviour {
	public float pushPower = 2.0f;

	void OnControllerColliderHit(ControllerColliderHit hit) {

		//if the player Controller hit the collider with the object's attached Rigidbody
		Rigidbody body = hit.collider.attachedRigidbody;

		//not hiting anything
		if(body == null || body.isKinematic) {
			return;
		}

		if(hit.moveDirection.y < -0.3) {
			return;
		}

		Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
		body.velocity = pushDir * pushPower;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
