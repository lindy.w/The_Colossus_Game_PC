﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnterTrigger : MonoBehaviour {


	public bool lighted = false;

	public GameObject[] fire;

	//GUI
	public Text GUIText;

	//showing text subtitles
	IEnumerator ShowMessage (string message, float delay) {
		GUIText.text = message;
		GUIText.enabled = true;
		yield return new WaitForSeconds(delay);
		GUIText.text = "";
		GUIText.enabled = false;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//if one of the fire is activiated, then lighted = true
		if (fire [0].activeInHierarchy == true || fire [1].activeInHierarchy == true || fire [2].activeInHierarchy == true) {
			lighted = true;
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == "Player") {
			
			if (lighted == false) {
				StartCoroutine (ShowMessage ("It's too dark! I need to find some matches to light up some candles.", 7));
			}
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player") {

		}
	}
}
