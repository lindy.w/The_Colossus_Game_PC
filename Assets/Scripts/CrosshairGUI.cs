﻿/*
CrosshairGUI.cs - wirted by ThunderWire Games * Script for Crosshair with Interact function
*fixed GUICrosshair.js
*/

using UnityEngine;
using System.Collections;

public class CrosshairGUI : MonoBehaviour {


public Texture2D m_crosshairTexture;
public Texture2D m_useTexture; //interact hand icon
public Texture2D m_observeTexture; //observe object icon
public float RayLength = 5f;
public float RayDoorLength = 10f;
public float RayPicLength = 20f;
public bool m_DefaultReticle; //Default using the CrossHair
public bool m_UseReticle;
public bool m_ObserveReticle; //new

//public bool m_ShowCursor = false;

public bool m_bIsCrosshairVisible = true;
private Rect m_crosshairRect;
private Ray playerAim;
private Camera playerCam;

//Get the barrel gameObject for accessing EnterTrigger
	public GameObject Barrel;
 
	void  Update (){
		playerCam = Camera.main;
		Ray playerAim = playerCam.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		RaycastHit hit;
		if (Physics.Raycast (playerAim, out hit, RayLength)) {
			if (hit.collider.gameObject.tag == "Match") {
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}
			if (hit.collider.gameObject.tag == "Drawer" || hit.collider.gameObject.tag == "ZDrawer" || hit.collider.gameObject.tag == "Drawer2") {
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}
			if (hit.collider.gameObject.tag == "Key") {
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}
			if (hit.collider.gameObject.tag == "Lamp") {
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}
			if (hit.collider.gameObject.tag == "Dairy") {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true; //show observe icon
				
			}
			if (hit.collider.gameObject.tag == "Map") {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true;
			}
			if (hit.collider.gameObject.tag == "Paper") {
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}
			if ( hit.collider.gameObject.tag == "Puzzle") {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true;
			}
			if (hit.collider.gameObject.tag == "Watch") {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true;
			}
			if (hit.collider.gameObject.tag == "Candle1" || hit.collider.gameObject.tag == "Candle2" || hit.collider.gameObject.tag == "Candle3") 
			{
				m_DefaultReticle = false;
				m_UseReticle = true;
				m_ObserveReticle = false;
			}

			if (hit.collider.gameObject.tag == "ToyBox" && Barrel.GetComponent<EnterTrigger>().lighted == true) {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true;
			}
			if (hit.collider.gameObject.tag == "WineBox") {
				m_DefaultReticle = false;
				m_UseReticle = false;
				m_ObserveReticle = true;
			}
				
		}

		if (Physics.Raycast (playerAim, out hit, RayDoorLength))
		{
				
			if(hit.collider.gameObject.tag == "Door")
				{
					m_DefaultReticle = false;
					m_UseReticle = true;
					m_ObserveReticle = false;
				}
			if (hit.collider.gameObject.tag == "Picture") 
				{
					m_DefaultReticle = false;
					m_UseReticle = false;
					m_ObserveReticle = true; //show observe icon
				}

		}

		else{
			m_DefaultReticle = true;
			m_UseReticle = false;
			m_ObserveReticle = false;
		}
	}

	void  Awake (){
		//cross hair
		if(m_DefaultReticle) {
		  m_crosshairRect = new Rect((Screen.width - m_crosshairTexture.width) / 2, 
								(Screen.height - m_crosshairTexture.height) / 2, 
								m_crosshairTexture.width, 
								m_crosshairTexture.height);
	    }

		//hand icon
	    if(m_UseReticle){
		  m_crosshairRect = new Rect((Screen.width - m_useTexture.width) / 2, 
								(Screen.height - m_useTexture.height) / 2, 
								m_useTexture.width, 
								m_useTexture.height);
	    }
		//observe icon
		if(m_ObserveReticle){
			m_crosshairRect = new Rect((Screen.width - m_observeTexture.width) / 2, 
				(Screen.height - m_observeTexture.height) / 2, 
				m_observeTexture.width, 
				m_observeTexture.height);
		}
	}
 
	void  OnGUI (){
		if(m_bIsCrosshairVisible)
		  if(m_DefaultReticle){
			GUI.DrawTexture(m_crosshairRect, m_crosshairTexture);
		 }
		  if(m_UseReticle){
			GUI.DrawTexture(m_crosshairRect, m_useTexture);
		 }
		if (m_ObserveReticle) {
			GUI.DrawTexture (m_crosshairRect, m_observeTexture);
		}
	}
}