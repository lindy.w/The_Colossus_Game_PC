﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	//references 
	public Transform InventoryCanvas; //get Canvas
	public Transform MapCanvas;
	public Transform DairyCanvas;
	bool showCursor = false;

	/*      GAME PUZZLE <-> FPC     */
	//For switching the cameras back
	public Transform PuzzleCamera, FPCamera;
	//For Hiding the chess arrows after switched back to FPC
	public GameObject [] selectors;
	//Game Text reset
	public Text GameText;


	//GUI
	public Text ItemText;

	public GameObject inventoryScript;

	public IEnumerator Time() {
		yield return new WaitForSeconds (.0001f); //wait 0.0001 seconds
		InventoryCanvas.gameObject.SetActive (false); //hide inventory
	}

	void Start() {
		//Lock and hide cursor
		Cursor.visible = (false);
		//Screen.lockCursor = true;
		Cursor.lockState = CursorLockMode.Locked;

		InventoryCanvas.gameObject.SetActive (true); //open inventory
		StartCoroutine(Time());

	}

	void Update() {

		//if we press esc button, cursor will occurs
		if (Input.GetKeyDown (KeyCode.Escape)) {
			showCursor = !showCursor;

			if (showCursor) {
				Cursor.visible = (true); //show Cursor
				Cursor.lockState = CursorLockMode.None; //Cursor can be freely moved around windows (for debugging)
			} else if (!showCursor) {
				Cursor.visible = (false); //show Cursor
				Cursor.lockState = CursorLockMode.Locked; //Cursor can be freely moved around windows (for debugging)
			}
		}

		//BackSpace button to switch back the camera to FPC
		if (Input.GetKeyDown (KeyCode.Backspace) && PuzzleCamera.gameObject.activeInHierarchy == true) {
			switchBackFPC ();
		}

		if (DairyCanvas.gameObject.activeInHierarchy == true) {
			GameObject.FindObjectOfType<CrosshairGUI> ().enabled = false;
			GameObject.FindObjectOfType<FirstPersonController> ().enabled = false;
		}
		//if we click the 'I' button , open and close the inventory view
		if (Input.GetKeyDown (KeyCode.I)) {

			if (InventoryCanvas.gameObject.activeInHierarchy == false) { // if the hierarchy is = false
				InventoryCanvas.gameObject.SetActive (true); //open panel
				if (GameObject.FindObjectOfType<CrosshairGUI>() != null)
				{
				 GameObject.FindObjectOfType<CrosshairGUI> ().enabled = false;
				}
				GameObject.FindObjectOfType<FirstPersonController> ().enabled = false;
				Cursor.visible = (true);
				//Screen.lockCursor = false;
				Cursor.lockState = CursorLockMode.Confined; //limited to the game view


			} else if (InventoryCanvas.gameObject.activeInHierarchy == true) {	// if the hierarchy is = true
				if (GameObject.FindObjectOfType<CrosshairGUI>() != null)
				{
					GameObject.FindObjectOfType<CrosshairGUI> ().enabled = true;
				}
				GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
				InventoryCanvas.gameObject.SetActive (false); //close panel
				ItemText.text = "";
				Cursor.visible = (false);
				//Screen.lockCursor = true;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
		
		if (Input.GetKeyDown (KeyCode.M) && inventoryScript.GetComponent<InteractScript> ().mapReview == true) {
			// Open the map Panel when it closed
			if (MapCanvas.gameObject.activeInHierarchy == false) {
				MapCanvas.gameObject.SetActive (true); //open panel
				if (GameObject.FindObjectOfType<CrosshairGUI> () != null) {
					GameObject.FindObjectOfType<CrosshairGUI> ().enabled = false;
				}
				GameObject.FindObjectOfType<FirstPersonController> ().enabled = false;
				Cursor.visible = (true);
				//Screen.lockCursor = false;
				Cursor.lockState = CursorLockMode.Confined; //limited to the game view
			}
			// Close the map panel when it's opened
			else if (MapCanvas.gameObject.activeInHierarchy == true) {
				MapCanvas.gameObject.SetActive (false); // close the panel
				GameObject.FindObjectOfType<FirstPersonController> ().enabled = true; // bring back the FPS
				if (GameObject.FindObjectOfType<CrosshairGUI> () != null) 
				{
					GameObject.FindObjectOfType<CrosshairGUI> ().enabled = true;
				}
				ItemText.text = "";
				Cursor.visible = (false);
				Cursor.lockState = CursorLockMode.Locked;
			}
		} 
	} // End of Update()

//		else if (MapCanvas.gameObject.activeInHierarchy == true && inventoryScript.GetComponent<InteractScript> ().mapOberving == false) {	
////			GameObject.FindObjectOfType<CrosshairGUI> ().enabled = true;
//			GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
//			MapCanvas.gameObject.SetActive (false); //close panel
//			ItemText.text = "";
//			Cursor.visible = (false);
//			//Screen.lockCursor = true;
//			Cursor.lockState = CursorLockMode.Locked;
//			}
//		}


	public void switchBackFPC() {
		PuzzleCamera.gameObject.SetActive(false);
		GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
		FPCamera.gameObject.GetComponent<AudioListener>().enabled = true;
		FPCamera.gameObject.SetActive(true);
		
		//Able the collider on Books
		foreach (GameObject books in GameObject.FindGameObjectsWithTag("Puzzle")) {
			books.GetComponent<Collider>().enabled = true; //able all items collider's to true
		}
		
		//show cursor
		Cursor.visible = (false); //show Cursor
		Cursor.lockState = CursorLockMode.Locked; 
		
		//Hide all selectors
		for (int i =0; i <selectors.Length; i++) {
			selectors[i].SetActive(false);
		}
		//Unable the Puzzle game script
		GameObject.FindObjectOfType<BookArrange>().enabled = false;
		//Hide the Puzzle game text
		GameText.enabled = false;
	}
}
