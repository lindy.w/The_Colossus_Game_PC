﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GUITextController : MonoBehaviour {
	//For Timer Counting
	public float startTime = 0;
	public float stageTime = 0;

	//GUI
	public Text GUIText;

	//showing text subtitles
	IEnumerator ShowMessage (string message, float delay) {
		GUIText.text = message;
		GUIText.enabled = true;
		yield return new WaitForSeconds(delay);	
		if (stageTime < 4 || stageTime >10) { 
			GUIText.enabled = false;
			GUIText.text = "";
		}
	}

	// Use this for initialization
	void Start () {
		//Start Timer 
		startTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
		stageTime = Time.time - startTime;

		if (stageTime >= 0 && stageTime <= 4) {
			//show GUI text
			StartCoroutine(ShowMessage("Where...Where am I?",4));
			//StartCoroutine(ShowMessage("Tones of woodboxes with wines...",4));
		}
		else if (stageTime > 4 && stageTime <= 9) {
			StartCoroutine (ShowMessage ("Is this my room? It looks creepy...Should I just look around?", 5));
			//StartCoroutine(ShowMessage("The master must be an alcoholic...",5));
		} 

		else if (stageTime > 9 && stageTime <=10) {
			GUIText.enabled = false;
			GUIText.text = "";
			GameObject.FindObjectOfType<FirstPersonController>().enabled = true;
			GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource> ().enabled = true;
		}
		Debug.Log (stageTime);
	}

	public void KeyMessage() {
		StartCoroutine (ShowMessage("A Key has been obtained.",3));

	}
}
