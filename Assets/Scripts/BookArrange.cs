﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Remember to remove the selector after solved puzzle!
public class BookArrange : MonoBehaviour {
	public GameObject object1;
	public GameObject object2;

	public GameObject [] selectors;

	Transform pObj1, pObj2;
	//public Transform arrow;
	public Text GameText;
	public bool selecting = true;

	void Start() {

		GameText.text = "Select a book you want to move: ";
	}

	public void OnEnable() {
		GameText.enabled = true;
		GameText.text = "Select a book you want to move: ";
		//reset
		object1 = null;
		object2 = null;
		selecting = true;
	}

	void Update() {
		//mouse left click up,
		if (Input.GetMouseButtonUp(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray,out hit,100)) {
				if (hit.collider.name == "book1" || hit.collider.name == "book2" || hit.collider.name == "book3"
				    || hit.collider.name == "book4" || hit.collider.name == "book5" || hit.collider.name == "book6") {
					//if it is selecting a first book,
					if(selecting) 
					{
						object1 = hit.collider.gameObject;

						//Hide all selectors
						for (int i =0; i <selectors.Length; i++) {
							selectors[i].SetActive(false);
							//change selectors' color to green
							selectors[i].GetComponent<Renderer>().material.color = Color.green;
						}

						//show the selector above the selected book
						if (object1.transform.parent.name == "bSlot1") {
							selectors[0].SetActive(true);
						}
						else if (object1.transform.parent.name == "bSlot2") {
							selectors[1].SetActive(true);
						}
						else if (object1.transform.parent.name == "bSlot3") {
							selectors[2].SetActive(true);
						}
						else if (object1.transform.parent.name == "bSlot4") {
							selectors[3].SetActive(true);
						}
						else if (object1.transform.parent.name == "bSlot5") {
							selectors[4].SetActive(true);
						}
						else if (object1.transform.parent.name == "bSlot6") {
							selectors[5].SetActive(true);
						}
						//Debug.Log(selectors.Length);
						selecting = false;
						GameText.text = "Select a book you want to swap with: ";
					}
					else if (!selecting) 
					{
						object2 = hit.collider.gameObject;

						//Hide all selectors
						for (int i =0; i <selectors.Length; i++) {
							selectors[i].SetActive(false);
							//change selectors' color to red
							selectors[i].GetComponent<Renderer>().material.color = Color.red;
						}

						//show the destination selector
						//show the selector above the selected book
						if (object2.transform.parent.name == "bSlot1") {
							selectors[0].SetActive(true);
						}
						else if (object2.transform.parent.name == "bSlot2") {
							selectors[1].SetActive(true);
						}
						else  if (object2.transform.parent.name == "bSlot3") {
							selectors[2].SetActive(true);
						}
						else  if (object2.transform.parent.name == "bSlot4") {
							selectors[3].SetActive(true);
						}
						else  if (object2.transform.parent.name == "bSlot5") {
							selectors[4].SetActive(true);
						}
						else  if (object2.transform.parent.name == "bSlot6") {
							selectors[5].SetActive(true);
						}
						swapBooks(object1,object2);
					}
					
				}
			}
		}
	}

	void swapBooks(GameObject obj1, GameObject obj2) {
		Vector3 tempPosition = object1.transform.position;
		object1.transform.position = object2.transform.position;
		object2.transform.position = tempPosition;

		pObj1 = object1.transform.parent;
		pObj2 = object2.transform.parent;

		Transform tempParent = pObj1;
		object1.transform.SetParent(pObj2);
		object2.transform.SetParent(tempParent);

		//reset
		object1 = null;
		object2 = null;
		

		selecting = true;
		GameText.text = "Select a book you want to move: ";
	}

	void checkResult() {

	}
}
