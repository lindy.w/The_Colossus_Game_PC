﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class exitGUI : MonoBehaviour {
	//references 
	public Transform canvas; //get Canvas

	public void Exit() {
		if (canvas.gameObject.activeInHierarchy == true) {	// if the hierarchy is = true
			if (GameObject.FindObjectOfType<CrosshairGUI> () != null) {
				GameObject.FindObjectOfType<CrosshairGUI> ().enabled = true;
			}
			GameObject.FindObjectOfType<FirstPersonController>().enabled = true;
			canvas.gameObject.SetActive (false); //close panel
			Cursor.visible = (false);
			//Screen.lockCursor = true;
			Cursor.lockState = CursorLockMode.Locked;
		}
	}
}
