﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class InventoryController : MonoBehaviour {
	public Transform selectedItem, selectedSlot, originalSlot, slots, canvas;
	public Transform[] Slots;

	//For papers UI
	public GameObject[] Papers;

	public bool canGrab = false;

	public GameObject[] Items; //all item in the game

	//variable to store and get itemCount 

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//When Selected

		if (Input.GetMouseButtonDown (0) && selectedItem != null) {
			canGrab = true;
			SelectedItemGrabbed(false); //unable Box Collider
			//Original slot
			originalSlot = selectedItem.parent;
			selectedItem.SetParent(canvas); //put on top

		}
	

		//When Dragging
		if (Input.GetMouseButton (0) && selectedItem != null && canGrab) {
			selectedItem.position = Input.mousePosition; //following mouse when dragging
		} 

		//When Released
		else if (Input.GetMouseButtonUp (0) && selectedItem != null) {
			if(selectedSlot == null) 
			{
					//if the original slot doesn't contain other item,
					if(originalSlot.childCount < 1)
						selectedItem.SetParent(originalSlot); //put back to original place
				else {
					selectedItem.SetParent(selectedSlot);
				}
			
			} else  {
				//if the selected slot contains other item,
				if(selectedSlot.childCount > 0) {
					selectedSlot.GetChild(0).SetParent(originalSlot); //swap with the contained item
					originalSlot.GetChild(0).localPosition = Vector3.zero;//reset position to parent
				}
					selectedItem.SetParent(selectedSlot); //put to selected new slot
			}
			canGrab = false;
			selectedItem.localPosition = Vector3.zero;//reset position to parent
			SelectedItemGrabbed(true); //able Box Collider
			selectedItem = null; //let the current item goes
		}
	}

	//New Functions

	void SelectedItemGrabbed(bool i) {

		foreach (GameObject item in GameObject.FindGameObjectsWithTag("Item")) {
			item.GetComponent<Collider>().enabled = i; //unable all items collider's to false
		}

	}

	public void AddPaper(string paperName) {
		for (int i = 0; i< Papers.Length;i++) 
		{
			if (Papers [i].name == paperName) {
				Papers [i].SetActive(true);
			}
		}
	}
	public void AddItem(string itemName) 
	{
		//GameObject CurrentItem;
		for (int i=0; i< Items.Length; i++) {
			// if the item is Match,
			if(Items[i].name == itemName && itemName == "Match") { //if the obtained object name = icon item name
				//Add the Item[i] to the inventory slot
				for (int i2 = 0; i< Slots.Length;i2++) 
				{
					//if the Slot has item,
					if (Slots[i2].childCount > 0) {
						//the slot already contains Match,
						if (Slots[i2].GetChild(0).name == "Match" && itemName == "Match")
						{
							// Add the Match Quatity
							Slots[i2].GetChild (0).GetChild(0).GetComponent<ItemCount>().AddItemCount();
							return;
						} 
					} else if (Slots[i2].childCount < 1) {
						GameObject newItem = (GameObject)Instantiate(Items[i]);
						//set the name of the cloned gameobject
						newItem.name = Items[i].name;
						newItem.transform.SetParent(Slots[i2],false); // add the Item prefab to the corresponding slot
						return; 
					}
				}
			} 
			 //Otherwise, if the item is other stuff,
			 else if (Items[i].name == itemName) {
				for (int i3 = 0; i< Slots.Length;i3++) 
				{
						//if the slot is empty
						if(Slots[i3].childCount < 1) {
							GameObject newItem = (GameObject)Instantiate(Items[i]);
							//set the name of the cloned gameobject
							newItem.name = Items[i].name;
							newItem.transform.SetParent(Slots[i3],false); // add the Item prefab to the corresponding slot
							return; 
					}
					
					//check next slots if the previous has item
				}
			}

			
		}
	}

	public bool useMatch() {
		//Search match in inventory slots
		for (int i = 0; i < Slots.Length;i++) {
			if(Slots[i].childCount > 0) {
				//if slot contains match item(s)
				if(Slots[i].GetChild (0).name == "Match") {
					//-1 match from the inventory
					Slots[i].GetChild(0).GetChild(0).GetComponent<ItemCount>().MinusItemCount();
					return true;
				} 
			}	
		}

		return false;
	}

	public int GetItemCount() {
		for (int i = 0; i < Slots.Length; i++) {
			//if the Slot has item,
			if (Slots[i].childCount > 0) {
				if (Slots[i].GetChild(0).name == "Match")
				{
					return Slots[i].GetChild(0).GetChild(0).GetComponent<ItemCount>().itemCount;
				}
			} else 
				return 0;
		}
		return 0;
	}

		
}
