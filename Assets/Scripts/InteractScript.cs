﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InteractScript : MonoBehaviour {
	//grab references
	public InventoryController inventoryController;

	public float interactDistance = 5f;

	int getItemTime;
	//GUI
	public Text GUIText;
	public Text InventoryText;
	public Text PaperText;
	public Image backImage;

	//references 
	public Transform DairyCanvas; //get Canvas
	public Transform MapCanvas; 

	//Map review function boolean
	public bool mapReview = false;
	public bool mapOberving = false;
	public int paperCount;

	//InGame Main Camera Array
	public GameObject[] mainCameras; //1: FirstPersonCamera 2:PuzzleCamera

	//The candle frames Array
	public GameObject[] fire;

	public GameObject Barrel;


	//showing text subtitles
	IEnumerator ShowMessage (string message, float delay) {
		GUIText.text = message;
		GUIText.enabled = true;
		yield return new WaitForSeconds(delay);
		GUIText.text = "";
		GUIText.enabled = false;
	}

	IEnumerator ShowRemindMessage (string message, float delay) {
		InventoryText.text = message;
		InventoryText.enabled = true;
		yield return new WaitForSeconds(delay);
		InventoryText.enabled = false;
		getItemTime = 2;
	}

	IEnumerator ShowPaperMessage (string message, float delay) {
		PaperText.text = message;
		PaperText.enabled = true;
		yield return new WaitForSeconds (delay);
		PaperText.enabled = false;
	}

	// Use this for initialization
	void Start () {
		getItemTime = 0;
		paperCount = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (getItemTime == 1) {
			//show GUI text
			StartCoroutine(ShowRemindMessage("An new item has obtained. Press 'I' can open Inventory.",5));
		}
		//hit the left mouse button - add raycast
		if (Input.GetKeyDown(KeyCode.Mouse0)) 
		{
			//origin : Camera position , direction: looking direction
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;


			if(Physics.Raycast(ray,out hit,interactDistance))
			{
					//if it is a door tag
				if (hit.collider.CompareTag ("Door")) {
					Door door = hit.collider.transform.parent.GetComponent<Door> ();
					//check if it's return null
					if (door == null)
						return;

					if (Inventory.keys [door.index] == true) {
						//access the parent script - ChangeDoorState() function
						door.ChangeDoorState ();
					} else if (Inventory.keys [door.index] == false) {
						//access the parent script - ChangeDoorState() function
						door.LockedState ();
						//show GUI text
						StartCoroutine (ShowMessage ("The door is locked. The door key must be somewhere...", 4));

					}
						
				} 
		
				if (hit.collider.CompareTag ("Paper")) {
					paperCount += 1;
					StartCoroutine (ShowPaperMessage ("Obtained " + paperCount + " /6 of poem fragments.", 5));
					Inventory.papers [hit.collider.GetComponent<Paper> ().index] = true;
					inventoryController.AddPaper(hit.collider.gameObject.name); //add the paper to intventory ui
					hit.collider.gameObject.GetComponent<Paper>().grabPaper();


					getItemTime = getItemTime + 1;
					Destroy (hit.collider.gameObject);
				}
				if (hit.collider.CompareTag ("Match")) {
					inventoryController.AddItem(hit.collider.gameObject.name); //add the obtained item into inventory table
					hit.collider.gameObject.GetComponent<Matches>().grabMatch();

					getItemTime = getItemTime + 1;
					Destroy (hit.collider.gameObject);
				}

			
			}
		}

		RaycastHit hit2;
		//Testing open drawer
		if (Physics.Raycast(transform.position,transform.forward,out hit2,5)) {

			if (hit2.collider.gameObject.name == "Alice_Painting" && Input.GetKeyDown (KeyCode.Mouse0)) {
				//change camera to Alice_Paint_Camera

			}

			if (hit2.collider.CompareTag("Dairy") && Input.GetKeyDown(KeyCode.Mouse0)) 
			{
				//show the dairy 2D layout 
				if (DairyCanvas.gameObject.activeInHierarchy == false) { // if the hierarchy is = false
					DairyCanvas.gameObject.SetActive (true); //open panel
					GameObject.FindObjectOfType<CrosshairGUI>().enabled = false;
					GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
					Cursor.visible = (true);
					//Screen.lockCursor = false;
					Cursor.lockState = CursorLockMode.Confined; //limited to the game view
				} 
			}

			if (hit2.collider.CompareTag ("Map") && Input.GetKeyDown (KeyCode.Mouse0)) {
				MapCanvas.gameObject.SetActive (true); //open panel
				mapOberving = true;
				GameObject.FindObjectOfType<CrosshairGUI>().enabled = false;
				GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
				Cursor.visible = (true);
				//Screen.lockCursor = false;
				Cursor.lockState = CursorLockMode.Confined; //limited to the game view
				mapReview = true;
			}

			if (hit2.collider.CompareTag ("Puzzle") && Input.GetKeyDown (KeyCode.Mouse0)) {
				//switch to puzzle Camera 
				mainCameras[1].gameObject.SetActive(true);
				mainCameras[0].GetComponent<AudioListener>().enabled = false;
				mainCameras[0].gameObject.SetActive(false);
				//close FPC with camera + The Crosshair GUI
				GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
				//unable the collider on Books
				foreach (GameObject books in GameObject.FindGameObjectsWithTag("Puzzle")) {
					books.GetComponent<Collider>().enabled = false; //unable all items collider's to false
				}
				//active the puzzle game 
				//show cursor
				Cursor.visible = (true); //show Cursor
				Cursor.lockState = CursorLockMode.None; 
				GameObject.FindObjectOfType<BookArrange>().enabled = true;
				GameObject.FindObjectOfType<BookArrange>().OnEnable();
			}
			if (hit2.collider.CompareTag ("Key") && Input.GetKeyDown (KeyCode.Mouse0)) {
				Inventory.keys [hit2.collider.GetComponent<Key> ().index] = true; //get keys and change its index bool to T
				inventoryController.AddItem (hit2.collider.gameObject.name); //add the obtained item into inventory table
				hit2.collider.gameObject.GetComponent<Key> ().grabKey ();

				getItemTime = getItemTime + 1;
				Destroy (hit2.collider.gameObject); //obtain the key and make it disappear
			}
			if (hit2.collider.CompareTag ("Watch") && Input.GetKeyDown (KeyCode.Mouse0)) {
				//show tips text to player
				//StartCoroutine (ShowMessage ("This must be brother’s pocket watch. He would never left behind this.", 4));
				GameObject.FindObjectOfType<CameraSettings> ().SwitchWatchCamera ();

			}

			if (hit2.collider.CompareTag ("Candle1") && Input.GetKeyDown (KeyCode.Mouse0)) {
				if (inventoryController.GetItemCount() >= 1) {
					//active fire 1
					fire [0].SetActive (true);
					//Match minus 1
					inventoryController.useMatch();
				} else {
					//not enough match
					StartCoroutine (ShowMessage("I don't have any match for lighting it up.",3));
				}

			}

			if (hit2.collider.CompareTag ("Candle2") && Input.GetKeyDown (KeyCode.Mouse0)) {
				if (inventoryController.GetItemCount() >= 1) {
					//active fire 1
					fire [1].SetActive (true);
					//Match minus 1
					inventoryController.useMatch();
				} else {
					//not enough match
					StartCoroutine (ShowMessage("I don't have any match for lighting it up.",3));
				}
			}

			if (hit2.collider.CompareTag ("Candle3") && Input.GetKeyDown (KeyCode.Mouse0)) {
				if (inventoryController.GetItemCount() >= 1) {
					//active fire 1
					fire [2].SetActive (true);
					//Match minus 1
					inventoryController.useMatch();
				} else {
					//not enough match
					StartCoroutine (ShowMessage("I don't have any match for lighting it up.",3));
				}

			}

			if (hit2.collider.CompareTag ("WineBox") && Input.GetKeyDown (KeyCode.Mouse0)) {
				GameObject.FindObjectOfType<CameraSettings> ().SwitchWineBoxCamera ();
			}

			if (hit2.collider.CompareTag ("ToyBox") && Input.GetKeyDown (KeyCode.Mouse0)  && Barrel.GetComponent<EnterTrigger>().lighted == true) {
				hit2.collider.gameObject.GetComponent<Collider> ().enabled = false;
				GameObject.FindObjectOfType<CameraSettings> ().SwitchToyBoxCamera ();
			}
			//Drawers part
			if (hit2.collider.CompareTag("Drawer")) {
				Drawer drawer = hit2.collider.gameObject.GetComponent<Drawer>();

				//if the drawer is not yet opening or not yet finished opening
				if(!drawer.opening && !drawer.opened) {
					//show tips text to player
					StartCoroutine (ShowMessage ("Click to open drawer.", 2));

					//open the drawer
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						drawer.opening = true;
						//add back the key tag to Key2
						GUIText.text = "";
					}
				}
				else if (drawer.opened && !drawer.closing) {
					//close the drawer
					StartCoroutine (ShowMessage ("Click to close drawer.", 2));
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						drawer.closing = true;
						GUIText.text = "";
					}
				}
			}	

			else if (hit2.collider.CompareTag("Drawer2")) {
				Drawer2 drawer2 = hit2.collider.gameObject.GetComponent<Drawer2>();

				//if the drawer is not yet opening or not yet finished opening
				if(!drawer2.opening && !drawer2.opened) {
					//show tips text to player
					StartCoroutine (ShowMessage ("Click to open drawer.", 2));

					//open the drawer
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						drawer2.opening = true;
						//add back the key tag to Key2
						GUIText.text = "";
					}
				}
				else if (drawer2.opened && !drawer2.closing) {
					//close the drawer
					StartCoroutine (ShowMessage ("Click to close drawer.", 2));
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						drawer2.closing = true;
						GUIText.text = "";
					}
				}
			}

			else if (hit2.collider.CompareTag("ZDrawer")) {
				ZDrawer zdrawer = hit2.collider.gameObject.GetComponent<ZDrawer>();

				//if the drawer is not yet opening or not yet finished opening
				if(!zdrawer.opening && !zdrawer.opened) {
					//show tips text to player
					StartCoroutine (ShowMessage ("Click to open drawer.", 2));

					//open the drawer
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						zdrawer.opening = true;
						//add back the key tag to Key2
						GUIText.text = "";
					}
				}
				else if (zdrawer.opened && !zdrawer.closing) {
					//close the drawer
					StartCoroutine (ShowMessage ("Click to close drawer.", 2));
					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						zdrawer.closing = true;
						GUIText.text = "";
					}
				}

			} else {
				//GUIText.text = "";
			}
		}

	}


}
