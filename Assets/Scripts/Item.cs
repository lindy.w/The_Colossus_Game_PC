﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item : MonoBehaviour {
	public Transform Inventory;
	public InventoryController inventoryController;

	//GUI
	public Text ItemText;

	GameObject inventory;
	GameObject guitext;

	void Start() {
		inventoryController = GameObject.FindObjectOfType<InventoryController> ();
		inventory = GameObject.Find ("/Canvas/Inventory");
		Inventory = inventory.GetComponent<RectTransform> ();
		guitext = GameObject.Find ("/Canvas/ItemText");
		ItemText = guitext.GetComponent<Text>();
		ItemText.text = "";
	}

	void OnMouseEnter() {
		inventoryController.selectedItem = this.transform; //when hover, selected slot is current
		//check what type of the item is,
		if (this.gameObject.name == "father_key_1") {
			//write description text for item
			ItemText.text = "A key belongs to master suite... ";
		}
		else if (this.gameObject.name == "brother_key_2") {
			//write description text for item
			ItemText.text = "Seem like a bedroom's key.";
		}
		else if (this.gameObject.name == "basement_key_5") {
			//write description text for item
			ItemText.text = "Umm...What's this key for? Maybe is the place marked on map?";
		}
		else if (this.gameObject.name == "Match") {
			//write description text for item
			ItemText.text = "Can only use it when running out of light. ";
		}
	}
	
	void OnMouseExit() {
		if (!inventoryController.canGrab) {
			//if we cannot grab it
			inventoryController.selectedItem =null;
			ItemText.text ="";
		}
	}

}
