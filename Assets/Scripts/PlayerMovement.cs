﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public Rigidbody rb;
	Vector3 vel;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update () {
		vel = rb.velocity;
	}
		
	void OnTriggerStay(Collider hitTrigger) {
		if(hitTrigger.transform.tag == "StairGoingUp") {
			//check the direction : compare with the angle 
			if (!Input.GetButtonDown("Jump") && Vector3.Angle(vel, hitTrigger.transform.forward) < 90)
			{
				if (vel.y > 0)
					vel.y = 0;
			}
		}

		if(hitTrigger.transform.tag == "StairGoingDown") {
			if(!Input.GetButtonDown("Jump") && Vector3.Angle(vel, hitTrigger.transform.forward) < 90) 
			{
				rb.AddForce(0,-50,0);

			}
		}
	}
}
