﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Key : MonoBehaviour {

	public int index = -1; 


	public void grabKey() {
		transform.parent.GetComponent<AudioSource>().Play(); //play sound which located in parent "KEYS"
		GameObject.FindObjectOfType<GUITextController>().KeyMessage();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
