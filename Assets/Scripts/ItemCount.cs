﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemCount : MonoBehaviour {
	public int itemCount;

	// Update is called once per frame
	void Update () {
		if (itemCount <= 1) {
			this.GetComponent<Text>().text = ""; // if it has only 1 or least, no text show
		} else {
			this.GetComponent<Text>().text = itemCount.ToString();
		}
		if (itemCount <= 0) {
			//destroy the match icon from inventory slot
			Destroy(transform.parent.gameObject);
		}
	}

	public void AddItemCount() {
		itemCount += 1;
	}

	public void MinusItemCount() {
		itemCount -= 1;
	}
}
