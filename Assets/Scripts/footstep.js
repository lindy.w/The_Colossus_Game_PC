﻿
 var ground : AudioClip[]; //sound clip
 
 private var step : boolean = true;
 var audioStepLengthWalk : float = 0.2; //length for walking
 
 function OnControllerColliderHit (hit : ControllerColliderHit) {
 var controller : CharacterController = GetComponent(CharacterController);  
 	//go thought on tag
 	if (controller.isGrounded && controller.velocity.magnitude < 7 && controller.velocity.magnitude > 5 && hit.gameObject.tag == "Ground" && step == true
 	|| controller.isGrounded && controller.velocity.magnitude < 7 && controller.velocity.magnitude > 5 && hit.gameObject.tag == "Untagged" && step == true)
 	{
 		WalkOnGround();
 		step = false;
 	}
}

 	//Grounded//
 	function WalkOnGround() {
 		step = false;
 		GetComponent.<AudioSource>().clip = ground[Random.Range(0,ground.length)]; //play random range and length
 		GetComponent.<AudioSource>().volume = 1;
 		GetComponent.<AudioSource>().Play();
 		yield WaitForSeconds (audioStepLengthWalk); //stop for while
 		step = true;
 	}
 	@script RequireComponent(AudioSource)
