﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public int index = -1;
	public bool open = false; //init. as close at the beginning
	public float doorOpenAngle = 90f; //default as 90 degree
	public float doorCloseAngle = 0f;
	public float smooth = 2f; //the speed

	public AudioClip lockedClip,openClip;
	AudioSource audioSrc;
	// Use this for initialization
	void Start () {
		audioSrc = GetComponent<AudioSource>();
	}

	public void ChangeDoorState() {
		open = !open; //change the open/close state
		audioSrc.clip = openClip;
		audioSrc.Play();
	}

	public void LockedState() {
		audioSrc.clip = lockedClip;
		audioSrc.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(open) //if true
		{
			//Quaternion = Rotation, Euler = make sure the no. within the range of --360 to 360
			Quaternion targetRotation = Quaternion.Euler (0, doorOpenAngle, 0); //change the y-axis angle
			//apply Rotation
			transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation,smooth * Time.deltaTime); //What u want to rotate from, to
		} else //Close Door
		{
			//Quaternion = Rotation, Euler = make sure the no. within the range of --360 to 360
			Quaternion targetRotation = Quaternion.Euler (0, doorCloseAngle, 0); //change the y-axis angle
			//apply Rotation
			transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation,smooth * Time.deltaTime); //What u want to rotate from, to

		}
	}
}