﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class CameraSettings : MonoBehaviour {

	public Transform mainCamera;
	public Transform canvas;
	public bool AutoFindCamera = false;
	public bool AutoFindCanvas = false;
	int size = 300;
	//GUI
	public Text GUIText, PaperText;
	public Image backImage;
	//InGame Main Camera Array
	public GameObject[] mainCameras; 

	GameObject curCamera;

	public InventoryController inventoryController;


	//showing text subtitles
	IEnumerator ShowMessage (string message, float delay) {
		GUIText.text = message;
		GUIText.enabled = true;
		yield return new WaitForSeconds(delay);
		GUIText.text = "";
		GUIText.enabled = false;
	}

	IEnumerator ShowPaperMessage (string message, float delay) {
		PaperText.text = message;
		PaperText.enabled = true;
		yield return new WaitForSeconds (delay);
		PaperText.enabled = false;
	}

	//Seconds Delay function
	IEnumerator Wait(float seconds)
	{
		yield return new WaitForSeconds(seconds);
	}

	IEnumerator Process(float seconds) 
	{
		yield return StartCoroutine (Wait (seconds));
		StartCoroutine(ShowMessage("It was given by mama on Christmas Boxing Day before mother died.", 2));
	}

	// Use this for initialization
	void Start () {
		if (AutoFindCamera)
			mainCamera = GameObject.FindObjectOfType<Camera> ().transform;
		if (AutoFindCanvas)
			canvas = GameObject.FindObjectOfType<Canvas> ().transform;
		backImage.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		mainCamera.transform.GetComponent<Camera> ().orthographicSize = canvas.transform.GetComponent<RectTransform> ().localScale.x * size;
		//Switch back to FPC
		if (curCamera == mainCameras [2] && Input.GetKeyDown (KeyCode.Mouse0)) {
			curCamera.gameObject.SetActive (false);
			mainCameras [0].GetComponent<AudioListener> ().enabled = true;
			mainCameras [0].gameObject.SetActive (true);
			GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
		}

		//Switch back to FPC
		if (curCamera == mainCameras [3] && Input.GetKeyDown (KeyCode.Mouse0)) {
			curCamera.gameObject.SetActive (false);
			mainCameras [0].GetComponent<AudioListener> ().enabled = true;
			mainCameras [0].gameObject.SetActive (true);
			GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
		}

		//Switch back to FPC
		if (curCamera == mainCameras [4] && Input.GetKeyDown (KeyCode.Backspace)) {
			SwitchToyBoxBacktoFPC ();
		}

		//mouse left click up,
		if (Input.GetMouseButtonUp (0) && curCamera == mainCameras [4]) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, 100)) {
				if (hit.collider.name == "toybox_O7:polySurface3") {
					StartCoroutine (ShowMessage ("It was a plain brown ninepins, I remember we painted them into color with brushesm What a happy childhood!", 3));
				}
				if (hit.collider.CompareTag("Paper")) {
					mainCameras[0].gameObject.GetComponent<InteractScript>().paperCount += 1;
					StartCoroutine (ShowPaperMessage ("Obtained " + mainCameras[0].gameObject.GetComponent<InteractScript>().paperCount + " /6 of poem fragments.", 5));
					Inventory.papers [hit.collider.GetComponent<Paper> ().index] = true;
					inventoryController.AddPaper(hit.collider.gameObject.name); //add the paper to intventory ui
					hit.collider.gameObject.GetComponent<Paper>().grabPaper();
					Destroy (hit.collider.gameObject);
				}
			}
		}

	}

	public void SwitchWatchCamera() {
		//switch to watch Camera 
		mainCameras[2].gameObject.SetActive(true);

		mainCameras[0].GetComponent<AudioListener>().enabled = false;

		mainCameras[0].gameObject.SetActive(false);
		//close FPC with camera
		GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
		//currentCamera
		curCamera = mainCameras[2];
		//show tips text to player
		StartCoroutine (ShowMessage ("This must be brother’s pocket watch. He would never left behind this.", 3));
		//Wait for 2 seconds
		StartCoroutine (Process(3.0f));
	}

	public void SwitchWineBoxCamera() {
		//switch to wine Camera 
		mainCameras[3].gameObject.SetActive(true);
		//show tips text to player
		StartCoroutine (ShowMessage ("Lots of wineboxes, the master must be a alcoholic.", 3));
		mainCameras[0].GetComponent<AudioListener>().enabled = false;

		mainCameras[0].gameObject.SetActive(false);
		//close FPC with camera
		GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
		//currentCamera
		curCamera = mainCameras[3];
	}
		
	public void SwitchToyBoxCamera() {
		//switch to toy Camera 
		mainCameras[4].gameObject.SetActive(true);

		mainCameras[0].GetComponent<AudioListener>().enabled = false;

		mainCameras[0].gameObject.SetActive(false);
		//close FPC with camera
		GameObject.FindObjectOfType<FirstPersonController>().enabled = false;
		Cursor.visible = (true); //show Cursor
		Cursor.lockState = CursorLockMode.None; //Cursor can be freely moved around windows (for debugging)
		//currentCamera
		curCamera = mainCameras[4];
		backImage.enabled = true;
		backImage.gameObject.GetComponent<Button> ().enabled = true;
	}

	public void SwitchToyBoxBacktoFPC() {
		backImage.enabled = false;
		backImage.gameObject.GetComponent<Button> ().enabled = false;
		curCamera.gameObject.SetActive (false);
		mainCameras [0].GetComponent<AudioListener> ().enabled = true;
		mainCameras [0].gameObject.SetActive (true);
		Cursor.visible = (false); //show Cursor
		Cursor.lockState = CursorLockMode.Locked; //Cursor can be freely moved around windows (for debugging)
		GameObject.FindObjectOfType<FirstPersonController> ().enabled = true;
		GameObject.FindGameObjectWithTag ("ToyBox").gameObject.GetComponent<Collider> ().enabled = true;
	}

}
