﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour {
	public Transform Inventory;
	public InventoryController inventoryController;

	void Start() {
		//inventoryController = GameObject.FindObjectOfType<InventoryController> ();
	}
	void OnMouseEnter() {
		inventoryController.selectedSlot = this.transform; //when hover, selected slot is current
	}

	void OnMouseExit() {
		inventoryController.selectedSlot = null;
	}

}
