﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {

	public static bool[] keys = new bool[10]; //default total 9 doors, 4 locked = 4 keys, 1 locked = main entrance
	public static bool[] papers = new bool[6]; //6 paper fragments "F.A.T.H.E.R"

	// Use this for initialization
	void Start () {
		keys [0] = true; //alice room (unlocked)
		keys [1] = false; //father room (locked)
		keys [2] = false; //brother room (locked)
		keys [3] = true; //reading room door (unlocked)
		keys [4] = true; //dining room door (unlocked)
		keys [5] = false; //door to basement (locked)
		keys [6] = true; //basement middle room (unlocked)
		keys [7] = true; //second last room at basement (unlocked)
		keys [8] = false; //last room at basement (locked)
		keys [9] = true; // main entrance (locked)
		//keys [3-9]

		//paper fragments
		for (int i = 0; i < papers.Length; i++) {
			papers [i] = false;
		}

	}
	
	// Update is called once per frame
	void Update () {
	}
}
