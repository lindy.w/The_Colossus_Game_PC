﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class changePages : MonoBehaviour {
	//4pages sprites
	public Sprite [] page = new Sprite[3];
	//set initialized page no.
	int page_num = 0;
	//GUIText
	public Text GUIText;

	void Update() {
		if (page_num == 0) {
			GUIText.text = "This is the picture which taken on our family day. Mother looks so gorgeous, shw was so hopeful and positive.";
		} else if (page_num == 1) {
			GUIText.text = "I have never seen mama again since that day. Daddy took us to a cemetery and told us mama had been to a place which is far away but beautiful. Daddy started drinking a lot since mama’s gone.";
		} else if (page_num == 2) {
			GUIText.text = "Father is such a hero! He definitely is the best daddy of the whole world. Even his work is very busy, but he takes care of us.";
		}
	}
	public void nextPage() {
		//From page 1 - 2, do show next page image
		if (page_num >= 0 && page_num < 2) 
		{
			this.gameObject.GetComponent<Image> ().sprite = page [page_num + 1];
			page_num += 1;
		}
	}

	public void backPage() {
		//From page 2-3, do show back page image
		if (page_num > 0 && page_num < 3) {
			this.gameObject.GetComponent<Image> ().sprite = page [page_num - 1];
			page_num -= 1;
		}
	}
		
}
