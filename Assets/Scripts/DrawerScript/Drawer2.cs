﻿using UnityEngine;
using System.Collections;

public class Drawer2 : MonoBehaviour {

	public float smoothTime = 0.1f;
	public float maxSpeed = 8f;
	public float openDistance = 0.5f;

	public bool opening;
	public bool opened;
	public bool closing;
	private Vector3 origin;//original position
	private Vector3 velocity = Vector3.zero; //0,0,0
	Vector3 targetPosition;

	// Use this for initialization
	void Start () {
		//store the current original position
		origin = transform.position;
	}

	// Update is called once per frame
	void Update () {
		//if opening = true
		if(opening){
			Vector3 targetPosition = origin + transform.right * openDistance; //transform.right = red axis (x)
			transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime, maxSpeed);

			if(Vector3.Distance(transform.position, targetPosition) < 0.1) {
				opening = false;
				opened = true; //opened drawer

				if (transform.childCount > 0) {
					//add back the key tag to Key2
					foreach (Transform child in transform) {
						child.gameObject.tag = "Key";
					}
				}
			}
		} else if (opened == true && closing) {
			targetPosition = origin - transform.right * openDistance/7.5f;
			transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime, maxSpeed);
			if(Vector3.Distance(transform.position, targetPosition) < 0.1) {
				closing = false; //not in process closing
				opened = false; //not opened drawer

			}
		}
	}
}
