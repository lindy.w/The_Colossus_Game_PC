﻿using UnityEngine;
using System.Collections;

public class ZDrawer : MonoBehaviour {

	public float smoothTime = 0.1f;
	public float maxSpeed = 8f;
	public float openDistance = 0.5f;

	public bool opening;
	public bool opened;
	public bool closing;
	//private Vector3 origin;//original position
	//private Vector3 velocity = Vector3.zero; //0,0,0
	Vector3 targetPosition;

	public Animation anim;

	// Use this for initialization
	void Start () {
		//store the current original position
		//origin = transform.position;
	}

	// Update is called once per frame
	void Update () {
		//if opening = true
		if(opening){
			
			anim ["Take 001"].speed = 1;
		
			anim.Play ("Take 001");

			if (transform.childCount > 0) {
				//add back the Key tag to Storeroom Key and father Key
				foreach (Transform child in transform) {
					if (child.gameObject.name == "basement_key_5" || child.gameObject.name == "father_key_1" || child.gameObject.name == "Key2")
						child.gameObject.tag = "Key";
					else if (child.gameObject.name == "p6")
						child.gameObject.tag = "Paper";
			
						
				}
			}
//			Vector3 targetPosition = origin + transform.forward * openDistance; //transform.forward = red axis (z)
//			transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime, maxSpeed);
//
//			if(Vector3.Distance(transform.position, targetPosition) < 0.1) {
			opening = false;
			opened = true; //opened drawer
//
//				if (transform.childCount > 0) {
//					//add back the paper tag to Paper
//					foreach (Transform child in transform) {
//						if (child.gameObject.name == "Key1") //Father's Key
//							child.gameObject.tag = "Key";
//						else
//							child.gameObject.tag = "Paper";
	//				}
		//		}
		//	}
		} else if (opened == true && closing) {
			anim ["Take 001"].speed = -1;
			//if animation finished, set the time to en before playing it backwards
			if (!anim.isPlaying) {
				anim ["Take 001"].time = anim ["Take 001"].clip.length;
			}

			anim.Play ("Take 001");
//			targetPosition = origin - transform.forward * openDistance/7.5f;
//			transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime, maxSpeed);
//			if(Vector3.Distance(transform.position, targetPosition) < 0.1) {
			if (transform.childCount > 0) {
				//add back the Key tag to Storeroom Key
				foreach (Transform child in transform) {
					if (child.gameObject.name == "Key3" || child.gameObject.name == "Key1")
						child.gameObject.tag = "Key";
				}
			}
			closing = false; //not in process closing
			opened = false; //not opened drawer

			}
	}
}
