﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapChangePages : MonoBehaviour {
	//4pages sprites
	public Sprite [] page = new Sprite[3];

	//set initialized page no.
	int page_num = 0;
	//GUIText
	public Text GUIText;

	void Update() {
		
	}
	public void nextPage() {
		//From page 1 - 2, do show next page image
		if (page_num >= 0 && page_num < page.Length) 
		{
			if (page_num != page.Length - 1) {
				this.gameObject.GetComponent<Image> ().sprite = page [page_num + 1];
				page_num += 1;
			} else {
				this.gameObject.GetComponent<Image> ().sprite = page [0];
				page_num = 0;
			}
		}
	}

	public void backPage() {
		//From page 2-3, do show back page image
		if (page_num >= 0 && page_num < page.Length) {
			if (page_num != 0) {
				this.gameObject.GetComponent<Image> ().sprite = page [page_num - 1];
				page_num -= 1;
			} else {
				this.gameObject.GetComponent<Image> ().sprite = page [page.Length - 1];
				page_num = page.Length - 1;
			}
		}
	}

}
