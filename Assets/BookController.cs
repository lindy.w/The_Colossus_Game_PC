﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class BookController : MonoBehaviour {
	public Transform[] Slots;
	public Transform[] Books; //all item in the game

	//Audio Clip
	public AudioClip activited;

	//boolean for checking the puzzle solved or not 
	public bool[] puzzleDone = new bool[6];

	private bool hasPlayed = false;

	// Use this for initialization
	void Start () {
		for (int i =0; i<puzzleDone.Length;i++) {
			puzzleDone[i] = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Check if all books are in the right slots
				if (Books[0].parent == Slots[0])
					puzzleDone[0] = true;
				if (Books[1].parent == Slots[1])
					puzzleDone[1] = true;
				if (Books[2].parent == Slots[2])
					puzzleDone[2] = true;
				if (Books[3].parent == Slots[3])
					puzzleDone[3] = true;
				if (Books[4].parent == Slots[4])
					puzzleDone[4] = true;
				if (Books[5].parent == Slots[5])
					puzzleDone[5] = true;

		if (puzzleDone[0] && puzzleDone[1] && puzzleDone[2] && puzzleDone[3] && puzzleDone[4] && puzzleDone[5])
		{
			//Call the trigger function
			Debug.Log ("Puzzle Solved");
			//play puzzle solved sound
			GameObject.FindObjectOfType<GameController> ().switchBackFPC();
			//play moving animation
			if (!hasPlayed) {
			transform.parent.GetComponent<Animation>().Play();
			transform.parent.GetComponent<AudioSource>().Play();	
				hasPlayed = true;
			}
		}

	}


}
