﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

	public GameObject howtoplay;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	     
	}

	public void onExit() {
		Application.Quit();   
	}

	public void onPlay() {
		Application.LoadLevel ("Gameplay");
	}

	public void onMore() {
		//show how to play
		howtoplay.SetActive(true);	

	}
}
